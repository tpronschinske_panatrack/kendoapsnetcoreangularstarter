import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserTransferStateModule, BrowserModule } from '@angular/platform-browser';
import { TransferHttpCacheModule } from '@nguniversal/common';
// i18n support
import { TranslateModule } from '@ngx-translate/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './containers/home/home.component';
import { NotFoundComponent } from './containers/not-found/not-found.component';
import { LinkService } from './shared/link.service';
import { UserService } from './shared/user.service';
import { routing } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent
  ],
  imports: [
    routing,
    RouterModule,
    CommonModule,
    HttpClientModule,
    TransferHttpCacheModule,
    BrowserTransferStateModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    // i18n support
    //TranslateModule.forRoot({
    //  loader: {
    //    provide: TranslateLoader,
    //    useFactory: createTranslateLoader,
    //    deps: [HttpClient, [ORIGIN_URL]]
    //  }
    //}),

   ],
  providers: [LinkService, UserService, TranslateModule],
  bootstrap: [AppComponent]
})
export class AppModuleShared {}
