﻿
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './containers/home/home.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'dashboard', component: HomeComponent },
];

export const routing = RouterModule.forRoot(appRoutes, { useHash: true, initialNavigation: 'enabled' });

