﻿using Asp2017.Server.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace AspCoreServer.Controllers
{
    public class HomeController : Controller {
        protected readonly IHostingEnvironment HostingEnvironment;
        public HomeController(IHostingEnvironment hostingEnv) => this.HostingEnvironment = hostingEnv;

        [HttpGet]
        public async Task<IActionResult> Index () {

            return View();
        }

     
    }
}
